import { Outlet } from "react-router"

const AttackContainer = () : JSX.Element => {
  return <Outlet />
} 

export default AttackContainer